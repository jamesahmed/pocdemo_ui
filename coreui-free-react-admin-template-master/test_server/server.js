var http = require('http');
var formidable = require('formidable');
var fs = require('fs');

http.createServer(function (req, res) {
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With, Content-Type, Accept, client_secret, client_id, access_token, Accept-Encoding, X-Forwarded-For"
);
  if (req.url == '/fileupload') {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var oldpath = files.filetoupload.path;
      var newpath = 'C:\\' + files.filetoupload.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        res.write('File uploaded and moved!');
        res.end();
      });
 });
  } else if (req.url == '/getData'){

    res.setHeader("Content-Type", "application/json");

    let data = {
      reports: [{id:1000,name:"fifa match 12",url:"https://www.cdn.com/fifamatch12"},
      {id:1001,name:"fifa match 13",url:"https://www.cdn.com/fifamatch13"},
      {id:1002,name:"fifa match 14",url:"https://www.cdn.com/fifamatch14"},
      {id:1003,name:"fifa match 15",url:"https://www.cdn.com/fifamatch15"},

      ]
    }

    res.end(JSON.stringify(data));
    
  }

  else {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="filetoupload"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
  }
}).listen(8090);


