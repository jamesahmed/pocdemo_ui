//   const express = require('express')
// const app = express()
// const port = 8000
// const mongoose = require("mongoose");
// const router = express.Router();
// const bodyParser = require("body-parser");
// const multer = require("multer");
// const upload = multer({ dest: './public/data/uploads/' })


// app.use(bodyParser.text());


// app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
// limit: '150mb',
// extended: true
// })); 

// app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));


// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })


// app.post('/', function (req, res) {
//   res.send('Got a POST request')
// })


// app.post('/upload/video', upload.array("file_data", 3), function (req, res) {


// // // Handle uploaded file
// console.log(req)

// var result = 
// {
// fileName:req.file
// };


//   res.json(result);
// })




// app.post('/add/record', function (req, res) {

// // Handle uploaded file

// console.log("Params:")
// console.log(req.query)


// console.log("Body Params:")
// console.log(req.body)

// var result = {
//   message:"File Received",
//   filePathOnServer:"something"
// };
//   res.json(result);
// })

// var path = require('path'),
//     fs = require('fs');
// const { Router } = require('express');

// function ensureDirectoryExistence(filePath) {
//     if (fs.existsSync(filePath)) {
//     return true;
//   }
//     fs.mkdirSync(filePath);
// }
// ensureDirectoryExistence(path.join(__dirname, 'uploads'));

// app.use( express.static(path.join(__dirname, 'public')))


// app.listen(port, () => {
//   console.log(`Example app listening at http://localhost:${port}`)
// })

// module.exports = router;



const express = require('express');
//const multer = require("multer");
const path = require('path');
const app = express()
const port = process.env.PORT || 8000

const uploadRoute = require('./routes/upload');

const bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
app.use(uploadRoute);


app.listen(port, () => {
    console.log('Server is up on port ' + port);
})
