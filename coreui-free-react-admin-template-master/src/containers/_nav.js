import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav = [{
            _tag: 'CSidebarNavItem',
            name: 'Dashboard',
            to: '/home',
            icon: < CIcon name = "cil-home"
            customClasses = "c-sidebar-nav-icon" / > ,
        },
        {
            _tag: 'CSidebarNavItem',
            name: 'Analyze Video',
            to: '/video/analyzevid',
            icon: 'cil-magnifying-glass'
        },
        //{
         //   _tag: 'CSidebarNavItem',
           // name: 'Heatmap',
            //to: '/view',
            //icon: 'cil-check'
        //},
        {
            _tag: 'CSidebarNavTitle',
            _children: ['Settings']
        },
        {
            _tag: 'CSidebarNavItem',
            name: 'Heatmap',
            to: '/settings/heatmap',
            icon: 'cil-grid'
        },
        {
            _tag: 'CSidebarNavItem',
            name: 'Configuration',
            to: '/settings/config',
            icon: 'cil-settings'
        },
    ]
export default _nav