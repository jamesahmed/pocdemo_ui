import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
      <span className="ml-1">&copy; 2021 </span>
        <a href="https://coreui.io" target="_blank" rel="noopener noreferrer">MatchPoint.ai, </a>
        <span className="ml-1">Inc., All Rights Reserved</span>
      </div>
      </CFooter>
  )
}

export default React.memo(TheFooter)
