import React, { useState } from 'react';
import './login.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'jquery/dist/jquery.min.js';
import $, { isEmptyObject } from 'jquery';

class Log_in extends React.Component {

    login()
    {
             console.log("state", this.state)
              if(document.getElementById("email","password").value.length == 0) {
               
            alert( "Email and password is required" ); 
              } else {
                window.location.href="/#/dashboard";
              }
       }

  componentDidMount(){
   //animation code
    $(document).ready(function(){
        $('.login-info-box').fadeOut();
        $('.login-show').addClass('show-log-panel');
    
    
    
    $('input[type="radio"]').on('change',function() {
    
        if($('#log-reg-show').is(':checked')) {
            $('.register-info-box').fadeIn();
            $('.login-info-box').fadeOut();
            
            $('.white-panel').removeClass('right-log');
            
            $('.login-show').addClass('show-log-panel');
            $('.register-show').removeClass('show-log-panel');
        }
        if($('#log-login-show').is(':checked')) {
            $('.register-info-box').fadeOut(); 
            $('.login-info-box').fadeIn();
            
            $('.white-panel').addClass('right-log');
            $('.register-show').addClass('show-log-panel');
            $('.login-show').removeClass('show-log-panel');
            
        }
    });
});
  }
  

  render() {
    return (

<div class="loginbody">
    <div class="login-reg-panel">
            <div class="login-info-box">
                <h2>Welcome Back!</h2>
                <p>To keep connected with us please login with your personal info</p>
                <label id="label-register" for="log-reg-show">Login</label>
                <input type="radio" name="active-log-panel" id="log-reg-show" value="log-reg-show" />
            </div>
                        
            <div class="register-info-box">
                <h2>MatchPoint.ai Video Analytics</h2>
                <p>Enter your personal details and start journey with us</p>
                <label id="label-login" for="log-login-show">Register</label>
                <input type="radio" name="active-log-panel" value="log-login-show" id="log-login-show" />
            </div>
                        
            <div class="white-panel">
              
                <div class="login-show">
                 
                <h2>LOGIN</h2>
                {/* <input type="text" placeholder="Email"/>
                <input type="password" placeholder="Password" />
                <input type="button" value="Login" />
                 */}
                 
                <input type="text" id="email" placeholder="Email" name="email" onChange={(e)=>{this.setState({email:e.currentTarget.value})}} style={{width:"25.2rem"}} required/>
                <input type="password" id="password" placeholder="Password" name="password" onChange={(e)=>{this.setState({password:e.currentTarget.value})}} style={{width:"25.2rem"}} required/>
                <input type="button" value="Login" onClick={()=>this.login()}/>
                

                </div>
                <div class="register-show">
                <h2>REGISTER</h2>
                <input type="text" placeholder="Email" />
                <input type="password" placeholder="Password" />
                <input type="password" placeholder="Confirm Password" />
                <input type="button" value="Register" />
                </div>
            </div>
            </div>  
</div>    
)
};
}
export default Log_in; 