import * as React from "react";
import * as ReactDOM from "react-dom";

import {
  ListView,
  ListViewHeader,
  ListViewFooter,
} from "@progress/kendo-react-listview";
import { Avatar } from "@progress/kendo-react-layout";


var dataSource = [];

let videos = [


 

];

const MyItemRender = (props) => {
  let item = props.dataItem;


  let imgPath = '/img/';

  if(item.value && item.value.indexOf("Analysis completed") > -1){
    imgPath = imgPath + "completed.png";

  }else if(item.value && item.value.indexOf("Analysis output") > -1){
    imgPath = imgPath + "completed.png";
  }else if(item.value && item.value.indexOf("creating bounding") > -1){
    imgPath = imgPath + "boundingbox.png";
  }else if(item.value && item.value.indexOf("Video uploaded") > -1){
    imgPath = imgPath + "upload.png";
  }else if(item.value && item.value.indexOf("rames") > -1){
    imgPath = imgPath + "frames.png";
  }else{
    imgPath = imgPath + "analyse.png";
  }
  item.imgPath = imgPath;
  return (
    <div
      className="row p-1 border-bottom align-middle"
      style={{
        margin: 0,
      }}
    >
      <div className="col-2">
        
           <img src={item.imgPath} class="eventImg"/>
        
      </div>
      <div className="col-10">
        <div
          style={{
            fontSize: 12,
            color: "#202020",
          }}
        >
          {item.value}
        </div>
      </div>
    </div>
  );
};


class EventList extends React.Component {
  
    
     
    constructor(props) {
    super(props);
     }


  getInitials = function (arg) {

    var string = arg.argumentText || arg.valueText;
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    
    if (names.length > 2) {
        initials += names[names.length - 2].substring(0, 1).toUpperCase() + names[names.length - 1].substring(0, 1).toUpperCase();
    }else if(names.length === 2){
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
  };

  componentDidMount() {


      const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      
    };
    console.log(requestOptions);
    fetch('http://34.131.182.107:3080/api/bn/events/list', requestOptions)
        .then(async response => {
            const dataRes = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (dataRes && dataRes.message) || response.statusText;
                return Promise.reject(error);
            }          


            videos = dataRes && dataRes.slice(0,10);
            this.forceUpdate();
        })
        .catch(error => {
            //this.setState({ errorMessage: error.toString() });
            console.error('There was an error!', error);
        });
      }
      render() {

        return (
            <div>
              <ListView
                data={videos}
                item={MyItemRender}
                style={{
                  width: "100%",
                }}
              />
            </div>
          );
        }

}

export default EventList;