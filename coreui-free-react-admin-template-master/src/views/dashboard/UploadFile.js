import React,{Component} from 'react';
import axios, {post} from 'axios';
import { BrowserRouter as Router, Route, Link, Redirect, Switch} from "react-router-dom";

import {
  CImg,
  CLabel,
  CCol,
  CSwitch,
  CFormGroup
} from '@coreui/react'

class UploadFile extends Component{
  constructor(props) {
    super(props);
    this.state= {
      File: ''
    }
  }

onChange(e){
  let files=e.target.files;
  
  let reader= new FileReader();
  reader.readAsDataURL(files[0]);

  reader.onload=(e)=>{
    const url= "localhost:8080/fileupload";
    const formData = { file: e.currentTarget.result}
    return post(url, formData)
        .then(response => console.log("result", response))
  }
}

  render() {
    return(
      <div onSubmit={this.onFormSubmit}>
     <h3>UploadFile</h3>
      <input type="file" name="file" onChange={(e)=>this.onChange(e)}/>
      <br/><br/>
        <CFormGroup row>
                  <CCol sm="12">
                  <CSwitch
                      className="mr-1"
                      color="primary"
                      defaultChecked
                      sm="1"
                    />
                    
                    <CImg width="60px" src="/img/nike.png" sm="1"/>
                    <CLabel sm="1">Nike</CLabel>
                    <CCol xs="1"/>
                    
                    <CSwitch
                      className="mr-1"
                      color="primary"
                      defaultUnchecked
                      sm="1"
                    />
                    
                    <CImg width="60px" src="/img/adidas.png" sm="1"/>
                    <CLabel sm="1">Adidas</CLabel>
                    <CCol xs="1"/>
                    
                    <CSwitch
                      className="mr-1"
                      color="primary"
                      defaultUnchecked
                      sm="1"
                    />
                    
                    <CImg width="60px" src="/img/puma.png" sm="1"/>
                    <CLabel sm="1">Puma</CLabel>

                    <CCol xs="1"/>
                    
                    <CSwitch
                      className="mr-1"
                      color="primary"
                      defaultUnchecked
                      sm="1"
                    />
                    
                    <CImg width="60px" src="/img/emirates.png" sm="1"/>
                    <CLabel sm="1">Emirates</CLabel>
                  </CCol>
              </CFormGroup>
             
      </div>
    )
  }
}
export default UploadFile;

