import React,{Component} from 'react'
import "c3/c3.css";
import EventList from "./events"
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CFormGroup,
  CInputFile,
  CRow,
  CSwitch,
  CImg,
  CLabel
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'
import Pie from './pie';
import Chart from './barchart'

const BasicForms = () => {
  // const [collapsed, setCollapsed] = React.useState(true)
  // const [showElements, setShowElements] = React.useState(true)

       


  return (
    <>
     


 <CRow>
        <CCol xs="12" sm="4">
          <CCard>
            <CCardHeader>
            Top 5 brands of the week
              <small> </small>
              
            </CCardHeader>
            <CCardBody>
             

            
                  <Chart/>
           

            </CCardBody>
            </CCard>
        </CCol>



        <CCol xs="12" sm="4">
          <CCard>
            <CCardHeader>
            Videos analysed by category
              <small> </small>
              
            </CCardHeader>
            <CCardBody>
             
              <Pie/>
           
            </CCardBody>
            </CCard>
        </CCol>


        <CCol xs="12" sm="4" >
          <CCard>
            <CCardHeader>
            Recent events
              <small> </small>
              
            </CCardHeader> 
            <CCardBody>

           <EventList/>
           

            </CCardBody>
          </CCard>
        </CCol>
</CRow>

    </>
  )
}
export default BasicForms;
