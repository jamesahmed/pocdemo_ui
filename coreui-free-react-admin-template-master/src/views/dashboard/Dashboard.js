import React, { lazy } from 'react'
import {
  // CBadge,
  // CButton,
  // CButtonGroup,
  // CCardHeader,
  // CProgress,
  // CCallout
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,

} from '@coreui/react'
import BasicForms from './UploadVideo';

const WidgetsDropdown = lazy(() => import('../widgets/WidgetsDropdown.js'))
const WidgetsBrand = lazy(() => import('../widgets/WidgetsBrand.js'))

const Dashboard = () => {
  return (
    <>
      <WidgetsDropdown />
      <CCard>
         <CCardBody>
           <BasicForms/>
               </CCardBody>
        <CCardFooter>
          <CRow className="text-center">
           
          </CRow>
        </CCardFooter>
      </CCard>

      
    </>
  )
}
export default Dashboard;
