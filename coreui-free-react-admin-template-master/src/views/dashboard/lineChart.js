import { count, ticks } from "d3-array";
import React from "react";
import { Line } from "react-chartjs-2";
import ReactDOM from "react-dom";

const colorPallette = ["rgba(235, 55, 52)","rgba(18, 148, 24)","blue","teal","orange","black","yellow","purple"]
Window.myVariable = data
let branddata = {

    };

let data = {
   
 labels:[


 ],
    datasets: [
  ]
};


class LineChart extends React.Component {

  constructor(props){
    super(props);

  
  }

  componentWillUnmount() {
       branddata = {};
       this.forceUpdate();
  }
  componentDidMount(){

      
    branddata = {};
   
   
    let linechartData = this.props.data;
    data.labels=[];
    if(linechartData && linechartData.length > 0){

      linechartData.map(function(item,index){
        if(item.frameId){
          data.labels.push(item.frameId.toString());
        }
        if(item.scores && item.scores.score){
          var count = 0;



          for (const [ind, it] of Object.entries(item.scores.score)) {
            console.log(ind, it);

            if(!branddata.hasOwnProperty(ind)){

              branddata[ind] = {
                label: ind,
                fill: false,
                lineTension: 0,
                backgroundColor: colorPallette[count],
                borderColor: colorPallette[count],
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: colorPallette[count],
                pointBackgroundColor: colorPallette[count],
                pointBorderWidth: 2,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: colorPallette[count],
                pointHoverBorderColor: colorPallette[count],
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                spanGaps: true,
                data:[],
               
              };
              count++;
              
            }

            branddata[ind].data.push(it);
            

          }
          
        }
      
      });
     
          } 
          

          var options = {
            onClick: function(evt,element) {

              var xPoint = evt.x;
              var maxPoint = 1;
              if(data.labels && data.labels.length > 0){
                maxPoint = parseInt(data.labels[data.labels.length-1]);  
              }
              
              
              var v = document.getElementById("video1");
              var v2 = document.getElementById("video2");
              if(v && v2){
                v.currentTime =  xPoint * v.duration / maxPoint;
                v2.currentTime = xPoint * v2.duration / maxPoint;

                
                setTimeout(function(){

                v2.play(); 
                //setTimeout(function(){v.pause();},2000);
                v.play(); 
                //setTimeout(function(){v2.pause();},2000);

                },1000);
               

              }

            },
        
            title: {
              display: true,
              fontSize: 20
            },
            legend: {
              display: false,
              spanGaps:false,
              
            },
               scales: {
                  xAxes: [
                  {
                    ticks: { 
                      display: false,
                     }
                  }
                ]},
                    
              yAxes: [
                {
                  ticks: {
                    display: false,
                      callback: function(value, index, values) {
                      return value;
                    }
                  }
                }
              ],
               
            }
         
    console.log(branddata);
    data.datasets = Object.values(branddata);
    const mountNode = document.getElementById('linechartc3');

        ReactDOM.render(

          <Line data={data} options={options} />, mountNode
          );

        this.forceUpdate();

        setTimeout(this.forceUpdate,1000);

    
       
           
        
  }

  render(){
    return (
    <div>
      {/* <h2>Brand Comparison</h2> */}
      <div id="linechartc3"/>
      
    </div>
  );

  }
  
}

// ReactDOM.render(<LineChart />, document.getElementById("root"));
export default LineChart