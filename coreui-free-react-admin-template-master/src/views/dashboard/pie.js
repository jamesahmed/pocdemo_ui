import React, { useState } from 'react'
import ReactApexChart from 'react-apexcharts'

// var pie = {data: {label:'null',count:1}};
class Pie extends React.Component {

constructor(props) {
    super(props);
    this.state = {
      
      series: [],
      options: {
        labels: [],
        legend: {
              position: 'bottom',
            },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 400,
              },
            }
        }]
      },
     };
  }

componentDidMount() {
      const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      
    };
    console.log(requestOptions);
    fetch('http://34.131.182.107:3080/api/bn/report/stats', requestOptions)
        .then(async response => {
            const dataRes = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (dataRes && dataRes.message) || response.statusText;
                return Promise.reject(error);
            }          
            for (const dataObj of dataRes){
              this.state.options.labels.push(dataObj.category)
              this.state.series.push(parseInt(dataObj.count))    
          }
              this.forceUpdate();
       })
        .catch(error => {
             console.error('There was an error!', error);
        });
  }
    render() {
      return (
             <div id="chart">
               <ReactApexChart options={this.state.options} series={this.state.series} type="pie"  style={{width:"32rem"}} />
          </div>
      );
    }
  }
export default Pie
