import React from "react";
import ReactDOM from "react-dom";
import C3Chart from "react-c3js";
import "c3/c3.css";

class ColumnData extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        json: {
          //Comment any one to check the error state
          Nike: [30, 200, 100, 400, 150, 250],
          Adidas: [130, 100, 140, 200, 150, 50],
          Nestle: [135, 130, 160, 260, 130, 150]
        },
       type: "bar",
        }
    };
  }

  render() {
    return (
      <div>
        <C3Chart {...this.state} />
      </div>
    );
  }
}


 
export default ColumnData