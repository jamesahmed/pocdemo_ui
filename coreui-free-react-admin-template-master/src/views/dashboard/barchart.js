import React from 'react';

import { Chart, SeriesTemplate, CommonSeriesSettings, Legend, Label, ArgumentAxis} from 'devextreme-react/chart';


var dataSource = [];
class App extends React.Component {
  
    
     
        constructor(props) {
    super(props);
     }


  getInitials = function (arg) {

    var string = arg.argumentText || arg.valueText;
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    
    if (names.length > 2) {
        initials += names[names.length - 2].substring(0, 1).toUpperCase() + names[names.length - 1].substring(0, 1).toUpperCase();
    }else if(names.length === 2){
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
  };

  componentDidMount() {


      const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      
    };
    console.log(requestOptions);
    fetch('http://34.131.182.107:3080/api/bn/video/stats', requestOptions)
        .then(async response => {
            const dataRes = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (dataRes && dataRes.message) || response.statusText;
                return Promise.reject(error);
            }          


            var brandList = dataRes.topBrands;
            brandList = brandList.map(function(cv,index){



                      return {'Brand':cv.split(":")[0],
                          'number':parseInt(cv.split(":")[1])
                      }
            });

            

            dataSource = brandList;
            this.forceUpdate();
        })
        .catch(error => {
            //this.setState({ errorMessage: error.toString() });
            console.error('There was an error!', error);
        });
      }
      render() {
    return (
      <Chart
        id="chart"
        palette="material"
        dataSource={dataSource}>

        
       
        <CommonSeriesSettings
          argumentField="Brand"
          valueField="number"
          type="bar"
          ignoreEmptyPoints={true}
        >


       


        </CommonSeriesSettings>
       <SeriesTemplate nameField="Brand" >
                     
        </SeriesTemplate>

        <ArgumentAxis> 
                    <Label
                        staggeringSpacing={10}
                        customizeText={this.getInitials}
                    />
                </ArgumentAxis>
       
       <Legend
                    position="bottom"
                    horizontalAlignment="center"
                    verticalAlignment="bottom"
                />

      </Chart>
    );
  }
}

export default App;
