import React from "react";
import { Formik, Form } from "formik";
import { Row, Col, Input, Button } from "reactstrap";
import { Container } from "react-bootstrap";


function FileUpload() {
    return(
         <div className="FileUpload">
               <Container>
                   <Row>
                       <Col>
                        <Formik 
                        initialValues={{video: ""}} 
                        onSubmit={(values) => {
                           let data = new FormData();

                           data.append("video", values.video);

                           return fetch(url, {
                               method: "post",
                               headers: new Headers({ Accept: "application/json"}),
                               body: data,
                           })
                           .then((response) => response.json())
                           .then((data) => console.log(data))
                           .catch((error) => console.log(error));
                        }}
                        >
                             {(formprops) => (
                                 <Form>
                                     <input 
                                     type="file"
                                      name="video"
                                      onChange={(event) =>
                                        formprops.setValues("video", event.terget.files[0])
                                    }
                                      />
                                     <Button type="submit">Submit</Button>
                                 </Form>
                             )}
                        </Formik>
                       </Col>
                   </Row>
               </Container>
         </div>
    )
}
export default FileUpload;