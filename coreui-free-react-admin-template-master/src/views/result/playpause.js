import { Show } from "devextreme-react/autocomplete";
import React from "react";
import { ReactDOM } from "react-dom";
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCardHeader,
  CProgress,
  CCallout,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,

} from '@coreui/react';

var count = 0;


var videoLength = 0;
var numFrames = 0;

class Video extends React.Component {
  

  constructor(props) {
        super(props);
        
        console.log(props.linechartData);
        numFrames = props.linechartData.length;
        if(this.props.linechartData && this.props.linechartData.length > 0){
          this.linechartData = this.props.linechartData.slice();
        }else{
          this.linechartData = [];
        }
        
        
    }

    componentWillMount(){
      this.brands=[];
      this.brandcounts=[];


    }

   
  playVideo = () => {
    this.brands=[];
    this.brandcounts=[];
    requestAnimationFrame(this.draw)
    
    // You can use the play method as normal on your video ref
    var vid1 = this.refs.vidRef1;
    var vid2 = this.refs.vidRef2;
    
    this.refs.vidRef2.play();
    this.refs.vidRef1.play();
    // this.interval = setInterval(() => this.tick(), 1000);
    console.log(vid1.currentTime);
    console.log(vid2.currentTime);
  };

  pauseVideo = () => {
    // Pause as well
    var vid3 = this.refs.vidRef1;
    var vid4 = this.refs.vidRef2;
    this.refs.vidRef1.pause();
    this.refs.vidRef2.pause();
          
    console.log(vid3.currentTime);
    console.log(vid4.currentTime);
  };
 
  
  replayVideo = () => {
    var curVid = this.refs.vidRef1;
    var vid5 = this.refs.vidRef1;
    var vid6 = this.refs.vidRef2;
    
    curVid.pause();
    curVid.currentTime = '0';
    

    var curVid2 = this.refs.vidRef2;
    
    curVid2.pause();
    curVid2.currentTime = '0';
    curVid2.play();

    curVid.play();

    clearInterval(this.interval);

    console.log(vid5.currentTime);
    console.log(vid6.currentTime);
  };
   

  draw = () => {
  
  this.forceUpdate();
  var v = document.getElementById("video1");

  if(v && v.paused) {
    setTimeout(requestAnimationFrame,1000,this.draw);
    return;
  }

  
  
  if(v){
    var progress = Math.round(v.currentTime * 100 / v.duration) ;
    var dataindex = Math.floor(numFrames * progress / 100);
    if(this.brands && this.brands.length ==0 && this.linechartData[dataindex]){
      this.brands = Object.keys(this.linechartData[dataindex].scores.score);
    }
    console.log(dataindex);
    if(!isNaN(dataindex) && this.linechartData[dataindex]){
      this.brandcounts = Object.values(this.linechartData[dataindex].scores.score);
      console.log(this.linechartData[dataindex].scores.score);
    }
  }
  
  count = count + 1;
  
    //requestAnimationFrame(this.draw);
    setTimeout(requestAnimationFrame,20,this.draw);
  };

  handleTimeUpdate = () => {



    count = this.currentTime;

  };


 
  render = () => {
    return (
     <>
      <CRow>
        <CCol xs="12" sm="12">
          <div>
             <center>
                <video style={{width:'550px'}}
                   ref="vidRef1"
                   src={this.props.video1}
                   type="video/mp4"
                   id="video1"
                   ontimeupdate={this.handleTimeUpdate}
                   />
                &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
                <video style={{width:'550px'}}
                   ref="vidRef2"
                   src={this.props.video2}
                    type="video/mp4"
                   id="video2"
                   />
             </center>
             <div>
                <center>
                   <button onClick={this.playVideo} style={{backgroundColor: "#0fd662", border: "none", color:"white", height:'2rem', width:'7rem'}}>
                   PLAY
                   </button> &nbsp;
                   <button onClick={this.pauseVideo} style={{backgroundColor: "#deba2a", border: "none",color:"white", height:'2rem', width:'7rem'}}>
                   PAUSE
                   </button>
                   &nbsp;
                   <button onClick={this.replayVideo} style={{backgroundColor: "#eb5850", border: "none",color:"white", height:'2rem', width:'7rem'}}>
                   REPLAY
                   </button>
                </center> 
             </div>
          </div>
        </CCol>
      </CRow>

       <CRow>
       <CCol xs="12" sm="3">
       </CCol>
        <CCol xs="12" sm="6">
          <div id="counters" align="center" style={{display:"inline-flex",align:"center"}}>
            
             {this.brands.map((name,i) => (
              <div >
               
               <h2 style={{display:"inline-flex",marginRight:"20px",marginBottom:"20px",marginTop:"20px"}}>
               {this.brandcounts[i]}</h2><h4 style={{display:"inline-flex",marginRight:"20px",marginBottom:"20px",marginTop:"20px"}}>{name}</h4>
              </div>

            ))} 
              
   
            
          </div>
        </CCol>
      </CRow>
     </>
    );
    
  };
  
}

export default Video;