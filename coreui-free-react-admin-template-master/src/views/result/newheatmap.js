import React from "react";
import HeatMap from "react-heatmap-grid";
import { PanelBar, PanelBarItem } from "@progress/kendo-react-layout";
import './newheatmap.css'
import './panelbar.css'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCardHeader,
  CProgress,
  CCallout,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,

} from '@coreui/react';

let heatmaps=[];
let titles = [];


let state = {
    selected: 2
  };

let handleSelect = function(e) {
    state.selected= e.selected;
  };

let videoId = "";
class Heatmap extends React.Component{
    
  constructor(props){
    super(props);
    videoId = props.videoId;
  }
   
  
componentDidMount() {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      
    };
    console.log(requestOptions);
    heatmaps = [];
    titles=[];
    fetch('http://34.131.182.107:3080/api/bn/video/heapmap/videoId/'+videoId, requestOptions)
        .then(async response => {
            let dataRes = await response.json();
            dataRes = dataRes.heapMapByBrand;
            for (var h in dataRes){
                // console.log(dataRes[h]);
                heatmaps.push(dataRes[h])
                titles.push(h)
            }
            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (dataRes && dataRes.message) || response.statusText;
    
              return Promise.reject(error);
          }          
        //   for (const dataObj of dataRes){
        //      data.push(parseInt(dataObj.heapMapByBrand))    
        // }
            this.forceUpdate();
     })
      .catch(error => {
           console.error('There was an error!', error);
      });
}


render(){
    

         let allHeatmaps = heatmaps.map(function(item,index){
                console.log(item)           

                let xLabels = ["1", "2", "3", "4","5","6","7","8","9"];
                let yLabels = ["", "", "", "",""];
                    return (
                 
                
                   <CCol sm="auto">
                    <div className="title">
                        <h2><center>{titles[index]}</center> </h2>
                            
                                    
                                       <center> <div className="heatmap">
                                             <HeatMap
                                              xLabels={xLabels}
                                              yLabels={yLabels}
                                              xLabelsLocation={"none"}
                                              xLabelWidth={0}
                                              yLabelWidth={0}
                                              data={item}
                                              squares
                                              height={66}
                                              onClick={(x, y) => alert(`Clicked ${x}, ${y}`)}
                                              cellStyle={(background, value, min, max, data, x, y) => ({
                                              background:`rgb(5, 234, 242, ${1 -0.99*(max - value) / (max - min)})`,
                                              fontSize: "15.5px",
                                              color: "#444",
                                              border:"0.5px solid grey",
                                            
                                              
                                             })}
                                            />
                                        </div></center>
                        </div>
                    </CCol>
                                   
                             
                        
                    )
        });
        
    return  <CRow fluid xs={{ cols: 3 }} sm={{ cols: 3 }} md={{ cols: 3 }}> {allHeatmaps}</CRow>
}

}
export default Heatmap