import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCardHeader,
  CProgress,
  CCallout,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,

} from '@coreui/react'
import ReactDOM from "react-dom";
import C3Chart from "react-c3js";
import "c3/c3.css";
import BasicForms from '../dashboard/UploadVideo';
import LineChart from '../dashboard/lineChart'
import ColumnData from '../dashboard/columnchart';
import './tvscreen.css'
import { ReactVideo } from "reactjs-media";
import { useAbsoluteLayout } from 'react-table';
import Video from './playpause';
import Heatmap from './newheatmap';
import queryString from 'query-string';
import Picture from '../picture/imageList';

let cumulativeScore = {};
let videoData = {};

const barChartData = {
                      
        data: {
            columns: [
                []
            ],
            type: 'bar',
            labels: {
              format:function (v, id, i, j) { return v + ''; }
            },
            colors: {
                data1: '#4DA7EF',
            }
        },
        bar: {
            width: 10
        },
        axis: {
            rotated: true,
            y: { 
                show: false,
            },       
            x:{
                type: 'category',
                categories: []
            }  
        },
        legend: {
            show: false
        },
        grid: {
            lines: {
              front: false
          }
        },
        tooltip: {
            show: false
        },
        };

        //heatmap
        const xLabels = [];
        

        // Display only even labels
        
        const yLabels = [];
        const emirates = [

            [10,10,100,14,15,14,100,15,17],
            [10,70,200,20,25,80,200,10,10],
            [80,10,20,10,20,10,20,70,10],
            [80,15,10,14,16,13,18,70,10],
            [10,10,10,20,10,20,20,10,10],
            
        ];

        const BNPP = [

          [10,20,30,200,250,200,30,20,10],
          [20,10,20,70,100,70,10,60,20],
          [20,20,20,10,10,15,25,15,10],
          [10,30,90,30,70,70,30,20,10],
          [10,30,10,30,30,30,30,10,20],
      ];

      //   const fila = [

      //     [0,0,0,0,0,0,0,0,0],
      //     [0,0,0,0,0,0,70,200,50],
      //     [0,0,0,0,0,0,20,0,0],
      //     [0,0,0,0,0,0,0,0,0],
      //     [0,0,0,0,0,0,0,0,0],
      // ];

let videoId = ""
let rid = ""
let originalVideoURL = "";
let analyzedVideoURL = "";
class Result extends React.Component{

  constructor(props){
    super(props);
     let params = queryString.parse(props.location.search)
     videoId = params.videoid;
     rid = params.rid;
    console.log(params);
  }


    componentWillUnmount(){

      //this.forceUpdate();

    }
    componentDidMount(){

      const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
     };
      console.log(videoId);

      fetch('http://34.131.182.107:3080/api/bn/video/report/videoId/' + videoId,requestOptions)
      .then(async response => {
        const dataRes = await response.json();
        if (!response.ok) {
         const error = (dataRes && dataRes.message) || response.statusText;
          return Promise.reject(error);
        }
        videoData = Object.assign(videoData,dataRes);
        console.log(videoData)

        originalVideoURL = videoData.videoUrl;
        analyzedVideoURL = videoData.analyzedUrl;

        cumulativeScore = dataRes.cumulativeScore;
        var barCharColumnLabels = Object.keys(videoData.logoCountByBrand);
        var barCharColumnValues = [0];

        for ( var c in Object.values(videoData.logoCountByBrand)){
          barCharColumnValues.push(Object.values(videoData.logoCountByBrand)[c]);
        }

        barChartData.axis.x.categories = barCharColumnLabels.slice();
        barChartData.data.columns[0] = barCharColumnValues.slice();

        const videoNode = document.getElementById('videopair');
        ReactDOM.render(

        <Video video1={originalVideoURL} video2={analyzedVideoURL} linechartData={cumulativeScore}/>
        , videoNode
          );

        const mountNode = document.getElementById('barchartc3');

        ReactDOM.render(

        <C3Chart 
                       data={barChartData.data} 
                        bar={barChartData.bar} 
                        axis={barChartData.axis} 
                        legend={barChartData.legend} 
                        grid={barChartData.grid}
                        tooltip={barChartData.tooltip}
                        style={{svg: {width: '100%'}}}>
                        </C3Chart>, mountNode
          );

          const linechartNode = document.getElementById('linechartelement');

        ReactDOM.render(

          <LineChart data={cumulativeScore}/>, linechartNode
          );

           





        this.forceUpdate();

      })
      .catch(error => {
        console.error('There was an error!', error);
      })
  }

      render(){

            return (
              
              <>
              <CRow>
                      <CCol xs="12" sm="12">
                        <CCard>
                          <CCardHeader>
                            Original Video
                            
                            
                           </CCardHeader>
                          <CCardBody>
                          
                                


                              <div id="videopair"/>
                            
                          </CCardBody>
                        </CCard>
                      </CCol>

                        <CCol xs="12" sm="6">
                        <CCard>
                          <CCardHeader>
                            Brand Visibility Comparison
                            <small></small>
                            
                          </CCardHeader>
                          <CCardBody id="lineChart">
                              <div className="border">
                              <div id="linechartelement"/>
                             
                              </div>
                          </CCardBody>
                        </CCard>
                      </CCol>

                      <CCol xs="12" sm="6">
                        <CCard>
                          <CCardHeader>
                            Logo Count by Brand
                            <small></small>
                          
                          </CCardHeader>
                          <CCardBody>
                          <div className="border">
                          <div id="barchartc3"/>
                        </div>
                          </CCardBody>
                        </CCard>
                      </CCol>

                      <CCol>
                        <CCard>
                          <CCardHeader>
                           Heatmap
                            <small></small>
                            
                          </CCardHeader>
                          <CCardBody>
                        
                            <Heatmap videoId={videoId}/>
                          </CCardBody>
                        </CCard>
                      </CCol>
                      </CRow>

                      <CCol xs="12" sm="12">
                        <CCard>
                          <CCardHeader>
                            High Value Picture
                            <small></small>
                            
                          </CCardHeader>
                          <CCardBody>
                          <div className="border">
                          <Picture rid={rid}/>
                          </div>
                          </CCardBody>
                        </CCard>
                      </CCol>

                      


              </>
          );
      }


    

}

export default Result