import React, {Component} from 'react';
import * as ReactDOM from "react-dom";
import {
    ListView,
    ListViewHeader,
    ListViewFooter,
  } from "@progress/kendo-react-listview";
  import { Avatar } from "@progress/kendo-react-layout";
import queryString from 'query-string';

  let picture=[];

const HighValue = (props) => {
    console.log(props);
    let item = props.dataItem;
        return (

      <div
        className="row p-2 border-bottom align-middle"
        style={{
          margin: 0,
        }}
      >
          
        
         <div className="col-2">
             <div className="text-uppercase"style={{
              fontSize: 14,
              color: "#454545",
              marginBottom: 0,
            }}>
          {item.logoName}</div>
        </div> 
  
        <div className="col-10">
        <div className="k-chip k-chip-filled" style={{height:"auto"}}>
          <div className="k-chip-content">
          

                <img src={item.url}/>
                 </div></div></div>
        
      </div>
    );
  };
  
let rid = "";
class Picture extends React.Component{

  constructor(props){
    super(props);
    //let params = queryString.parse(props.location.search)
     rid = props.rid;
    console.log(props);
  }

    
componentDidMount(){
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
     };
    console.log(requestOptions);
    fetch('http://34.131.182.107:3080/api/bn/score/list/reportId/' + rid,requestOptions)
      .then(async response => {
        const dataRes = await response.json();
        if (!response.ok) {
         const error = (dataRes && dataRes.message) || response.statusText;
          return Promise.reject(error);
        }
        picture = dataRes;

        this.forceUpdate();
  })
  .catch(error => {
    console.error('There was an error!', error);
  })
}
    render(){

        return (
          <div>
            <ListView
              data={picture}
              item={HighValue}
              style={{
                width: "100%",
              }}
            />
          </div>
        );
  
      }
  
}

export default Picture;