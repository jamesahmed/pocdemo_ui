import React, { Component } from 'react'
import { ReactVideo } from "reactjs-media";

class Video extends Component{
    render(){
        return(

            <div>
            <ReactVideo

                src="/video/08.mp4"

                poster="https://www.example.com/poster.png"
                primaryColor="red"
                // other props
            />
        </div>
        );
    }}

    export default Video;