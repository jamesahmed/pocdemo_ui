import React, { useReducer, useState } from 'react';
import axios from 'axios'

const formReducer = (state, event) => {
 return {
   ...state,
   [event.name]: event.value,
   
 }
}

function Form() {
  const [formData, setFormData] = useReducer(formReducer, {});
  const [submitting, setSubmitting] = useState(false);


    let getData = function(){

    console.log(formData)
    axios({
        'method':'POST',
        'url':'http://localhost:8000/addUser',
        'headers': {
            'content-type':'application/json'
        
      },
      'data': {
        'Name':formData.name,
        'file':formData.file
    }
    })
  }
  const handleSubmit = event => {
    event.preventDefault();
    setSubmitting(true);
    //fetch methos to invoke p,ost request
    
    getData();
    
    setTimeout(() => {
      setSubmitting(false);
    }, 3000);
  }

  const handleChange = event => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  }

  return(
    <div className="wrapper">
      <h1>How About Them Apples</h1>
      {submitting &&
       <div>
         You are submitting the following:
         <ul>
           {Object.entries(formData).map(([name, value]) => (
             <li key={name}><strong>{name}</strong>:{value.toString()}</li>
             
           ))}
            
         </ul>
       </div>
      }
      <form  enctype="multipart/form-data" onSubmit={handleSubmit}>
        <fieldset>
          <label>
            <p>Name</p>
            <input name="name" onChange={handleChange}/><br/>
            <p>choose</p>
            <input type="file" name="file" onChange={handleChange}/>
          </label>
        </fieldset>
        <button type="submit">Submit</button>
      </form>
    </div>
  )
 }

export default Form