import React, { Component } from 'react'
import { Dropdown } from 'react-bootstrap';
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CCollapse,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CFade,
    CForm,
    CFormGroup,
    CFormText,
    CValidFeedback,
    CInvalidFeedback,
    CTextarea,
    CInput,
    CInputFile,
    CInputCheckbox,
    CInputRadio,
    CInputGroup,
    CInputGroupAppend,
    CInputGroupPrepend,
    CDropdown,
    CInputGroupText,
    CLabel,
    CSelect,
    CRow,
    CSwitch,
    CImg
  } from '@coreui/react'
  import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'
import Video from './video';
import Form from './uploadfile';

const Report = () => {
    const [collapsed, setCollapsed] = React.useState(true)
    const [showElements, setShowElements] = React.useState(true)
  
    return (

  <>
      <CRow>
        <CCol xs="12" sm="6">
          <CCard>
            <CCardHeader>
            Analyze Video
            </CCardHeader>
           
            <CCardBody>
            
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>


       <CRow>
        <CCol xs="12" sm="6">
        <CCard>
          <CCardHeader>
            Report
            <small></small>
            <DocsLink name="-Input"/>
     
          </CCardHeader>
          <CCardBody>
          
          </CCardBody>
        </CCard>
      </CCol>
                  
                  <CCol xs="12" sm="6">
                  <CCard>
                    <CCardHeader>
                      Video Analysis
                      <small></small>
                      <DocsLink name="-Input"/>
                    </CCardHeader>
                    <CCardBody>
                    <Video/>
                    </CCardBody>
                    
                  </CCard>
                </CCol>
               
               
                <CCol xs="12" sm="50">
                  <CCard>
                    <CCardHeader>
                      Video Analysis
                      <small></small>
                      <DocsLink name="-Input"/>
                    </CCardHeader>
                    <CCardBody>
                      
                    </CCardBody>
                  </CCard>
                </CCol>
                
                </CRow>
        </>
        )
    }


    export default Report