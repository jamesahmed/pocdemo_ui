import React, {useState, useEffect, Component} from 'react';
import * as ReactDOM from "react-dom";

import {
  ListView,
  ListViewHeader,
  ListViewFooter,
} from "@progress/kendo-react-listview";
import { Avatar } from "@progress/kendo-react-layout";

let videos = [

    {
    "id": 1,
    "videoId": "070064a0d1574a3ab6eac98ff99b8d39",
    "label": "test",
    "email": "ssyed",
    "videoUrl": "https://res.cloudinary.com/dtihsirt7/video/upload/v1630208870/bzmwperrgjrutq0uxx7v.mp4",
    "thumbnailUrl": "https://res.cloudinary.com/dtihsirt7/video/upload/v1630208870/bzmwperrgjrutq0uxx7v.jpg",
    "overlayVideoUrl": null,
    "brands": [""],
    "status": 0
    },

];

const statusMap = ["In Progress","Completed","Failed"]

const MyItemRender = (props) => {
  console.log(props);
  let item = props.dataItem;
  if(!item.status) {
    item.status=0;
  }
  if(item.brands === null || !item.brands || item.brands === undefined) {
    item.brands = [""];
  }
  return (
    <div
      className="row p-2 border-bottom align-middle"
      style={{
        margin: 0,
      }}
    >
      <div className="col-2">
        <Avatar shape="badge" type="img">
          <img
            src={item.thumbnailUrl}
            height="30px"
            width="50px"
            object-fit="fill"
          />
        </Avatar>
      </div>
      <div className="col-4">
        <h2
          style={{
            fontSize: 14,
            color: "#454545",
            marginBottom: 0,
          }}
          className="text-uppercase"
        >
          {item.label}
        </h2>
        <div
          style={{
            fontSize: 12,
            color: "#a0a0a0",
          }}
        >
          {statusMap[item.status]}
        </div>
      </div>

       <div className="col-2">
        {item.label}
      </div> 

     

      <div className="col-2">
        <div className="k-chip k-chip-filled">
          <div onClick={() => {
            window.location.href = '/#/result?videoid=' + item.videoId + '&rid=' + item.id;
            }}
          className="k-chip-content">{item.messages} View Report</div>
        </div>
      </div>
    </div>
  );
};


 // <div className="col-2">
 //         {item.brands.reduce((acc, item) => { return acc + " " + item; })} 
 //      </div>

class ReportList extends React.Component{


constructor(props){
  super(props);
}
componentDidMount(){
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
     };
    console.log(requestOptions);
    fetch('http://34.131.182.107:3080/api/bn/report/list',requestOptions)
      .then(async response => {
        const dataRes = await response.json();
        if (!response.ok) {
         const error = (dataRes && dataRes.message) || response.statusText;
          return Promise.reject(error);
        }
        videos = dataRes;

        this.forceUpdate();
  })
  .catch(error => {
    console.error('There was an error!', error);
  })
}

render(){

      return (
        <div>
          <ListView
            data={videos}
            item={MyItemRender}
            style={{
              width: "100%",
            }}
          />
        </div>
      );

    }

  
}

export default ReportList;