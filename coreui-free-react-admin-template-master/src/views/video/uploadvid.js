import { useCallback } from "react";
import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CLink,
  CProgress,
  CRow
} from '@coreui/react'
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Form, Field, FormElement  } from "@progress/kendo-react-form";
import { Upload } from "@progress/kendo-react-upload";
import { MultiSelect } from "@progress/kendo-react-dropdowns";
import Select from 'react-select';
import './analyzevid.css'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReportList from './reportList'



const emailRegex = new RegExp(/\S+@\S+\.\S+/);
var enableSubmit = false;
const emailValidator = (value) =>
  emailRegex.test(value) ? "" : "Please enter a valid email.";
  // const brands = ["Nike","Emirates","BNP Paribas","Bank BGŻ BNP Paribas","Fila"];
  const brands = [
    { value: 'Nike', label: 'Nike' },
    { value: 'Emirates', label: 'Emirates' },
    { value: 'BNP Paribas', label: 'BNP Paribas' },
    { value: 'Bank BGŻ BNP Paribas', label: 'Bank BGŻ BNP Paribas' },
    { value: 'Fila', label: 'Fila' },
    { value: 'FC Barcelona', label: 'FC Barcelona' },
    { value: 'Pepsi', label: 'Pepsi' },
    { value: 'Qatar Airways', label: 'Qatar Airways' },
    { value: 'Pepsi Max', label: 'Pepsi Max' },
    { value: 'Sony', label: 'Sony' },
    { value: 'Mastercard', label: 'Mastercard' },
    ];


  const categories = [
    { value: 'Sports', label: 'Sports' },
    { value: 'News', label: 'News' },
    { value: 'Entertainment', label: 'Entertainment' },
    { value: 'Others', label: 'Others' },
  ];

  
var category = "";

const CustomCheckbox = (fieldRenderProps) => {
  const {
    validationMessage,
    visited,
    value,
    onChange,
    onFocus,
    onBlur,
    ...props
  } = fieldRenderProps;

  const onValueChange = useCallback(() => {
    onChange({ value: !value });

    console.log(value);
  }, [onChange, value]);
 
  return (
    <div onFocus={onFocus} onBlur={onBlur}>
      <label htmlFor={props.name}>{props.label}</label>
      <input
        type="checkbox"
        onChange={onValueChange}
        checked={value}
        id={props.id}
      />
      {visited && validationMessage && <span>{validationMessage}</span>}
    </div>
  );
};

var value =[];
var state = {};
var videoURL = "";
let thisObj = {};
class DataForm extends React.Component {


  constructor(props){
    super(props);
    thisObj=this;
    enableSubmit = false;
  }

  onSelect(event) {
    //setValue([...event.value]);
  }
  onSelectCategory(event) {
    console.log("Category selected");
    category="Sports";

  }
  
  handleSubmit(dataItem){


    if(videoURL && videoURL.length > 0){
      dataItem.url = videoURL;

      // window.location.reload(false);
//       ReactDOM.unmountComponentAtNode(document.getElementById('reportlist')
// )
      // ReactDOM.render(
      //   <ReportList id='reportlist'/>,
      //   document.getElementById('reportlist')
      // );
      const notify = () => toast("Your video is being analysed.");
      setTimeout(function(){window.location.reload(false);},5000);
      notify();

    }
    
    dataItem.brands = ["Nike","Emirates","BNP Paribas","Bank BGŻ BNP Paribas","Fila","FC Barcelona","Pepsi","Qatar Airways","Pepsi Max","Sony","Mastercard"];

    dataItem.category = category;
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(dataItem)
    };
    fetch('http://34.131.182.107:3080/api/bn/video/analyze', requestOptions)
        .then(response => response.json())
        .then(data => {
          enableSubmit = false;
        });

  }

    // On file select (from the pop up)
    onFileChange(event){
    
      // Update the state
      state =  {selectedFile: event.target.files[0]};

       // Create an object of formData
      const formData = new FormData();
    
      // Update the formData object
      formData.append(
        "file",
        state.selectedFile
      );
    
      // Details of the uploaded file
      console.log(state.selectedFile);
    
      // Request made to the backend api
      // Send formData object

      axios.post("http://34.131.182.107:3080/api/bn/video/file_upload", formData)
      .then(response => {
        console.log(response.data);
        videoURL = response.data.url;
        enableSubmit = true;

        thisObj.forceUpdate();

      });
    
    }
    
    // On file upload (click the upload button)
    onFileUpload(){
    
     


    };
    state = {
      selectedOption: null,
      selectedCategory: null
    };
    handleChange = selectedOption => {
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
    };

    handleChangeCategory = selectedCategory => {
      this.setState({ selectedCategory });
      console.log(`Option selected:`, selectedCategory);
    
    };
    render(){
      const { selectedOption } = this.state;
      const { selectedCategory } = {};
   
      return (
                 
          <Form
            initialValues={{ label: "", email: "" }}
            onSubmit={this.handleSubmit}
            validator={({ label, email }) => ({
              label: label ? "" : "Label is required",
              email: emailValidator(email)
            })}
            render={(formRenderProps) => (
              <FormElement>
                <fieldset>
                  <legend>Upload Video</legend>
                  <div>

                    <Field name="label" placeholder="Enter label" component="input" style={{ width: "47%" }} />
                    {formRenderProps.touched && formRenderProps.errors.label && (
                      <span>{formRenderProps.errors.label}</span>
                    )}
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <Field placeholder="Enter Email" name="email" type="email" component="input" style={{ width: "47%" }} />
                    {formRenderProps.touched && formRenderProps.errors.email && (
                      <span>{formRenderProps.errors.email}</span>
                    )}
                  </div>

                  <br />

                  <div className="row example-wrapper" style={{ width: "210%", height: "40px" }}>
                    <div className="col-xs-6 col-sm-3 example-col">
                      <Select
                        placeholder="Select Brands"
                        closeMenuOnSelect={false}
                        isMulti
                        value={selectedOption, this.value}
                        onChange={this.handleChange, this.onSelect}
                        options={brands} />


                    </div>

                    <div className="col-xs-6 col-sm-3 example-col">
                      <Select
                        placeholder="Select Category"
                        closeMenuOnSelect={false}
                        isMulti
                        value={selectedCategory, this.value}
                        onChange={this.handleChangeCategory, this.onSelectCategory}
                        options={categories} />
                    </div>

                  </div>

                </fieldset>
                <br /><br />

                <fieldset>

                  <div>

                    <input class="btn btn-secondary" type="file" onChange={this.onFileChange} style={{ width: "100%" }} />

                  </div>
                  <div>
                    <center>OR</center>
                  </div>
                  <div>

                    <Field className="field" placeholder="Enter Video URL" name="videourl" type="text" component="input" value={videoURL} />
                    {formRenderProps.touched && formRenderProps.errors.videourl && (
                      <span>{formRenderProps.errors.videourl}</span>
                    )}
                  </div>

                </fieldset>
                <div><br />
                  <button className="button btn-info"
                    type="submit"

                    onClick={formRenderProps.onFormReset}
                    style={{ border: "none" }}

                  >
                    Reset
                  </button>&nbsp;&nbsp;
                  <button
                    disabled={!formRenderProps.modified && enableSubmit}
                    className="button  btn-success" style={{ border: "none" }} type={"submit"} disabled={!formRenderProps.allowSubmit}>
                    Submit
                    </button>
                    <ToastContainer/>
                </div>

              </FormElement>
            )} />

    
    
  );

    }
}


export default DataForm;