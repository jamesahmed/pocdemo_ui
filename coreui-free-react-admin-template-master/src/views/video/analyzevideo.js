import React, { Component, useState } from 'react'

import { Dropdown } from 'react-bootstrap';

import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CCollapse,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CFade,
    CForm,
    CFormGroup,
    CFormText,
    CValidFeedback,
    CInvalidFeedback,
    CTextarea,
    CInput,
    CInputFile,
    CInputCheckbox,
    CInputRadio,
    CInputGroup,
    CInputGroupAppend,
    CInputGroupPrepend,
    CDropdown,
    CInputGroupText,
    CLabel,
    CSelect,
    CRow,
    CSwitch,
    CImg
  } from '@coreui/react'
  import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'
import DataForm from './uploadvid';
import ReportList from './reportList';

const Report = () => {
    const [collapsed, setCollapsed] = React.useState(true)
    const [showElements, setShowElements] = React.useState(true)
  
    return (

  <>
      <CRow>
        <CCol xs="12" sm="2">
        </CCol>
        <CCol xs="12" sm="8">
          <CCard>
            <CCardHeader>
            Analyze Video
            </CCardHeader>
           
            <CCardBody>
            

            <DataForm/>

            </CCardBody>
          </CCard>
        </CCol>


        </CRow>
        <CRow>
     
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
            Results
            </CCardHeader>
           
            <CCardBody>
            <div id="reportlist">
            <ReportList/>
            </div>
            </CCardBody>
          </CCard>
        </CCol>




      </CRow>


       
        </>
        )
    }


    export default Report