// import { CInputCheckbox } from '@coreui/react'
import React, { Component } from 'react'
import {
   CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CCollapse,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CFade,
    CForm,
    CFormGroup,
    CFormText,
    CValidFeedback,
    CInvalidFeedback,
    CTextarea,
    CInput,
    CInputFile,
    // CInputCheckbox,
    CInputRadio,
    CInputGroup,
    CInputGroupAppend,
    CInputGroupPrepend,
    CDropdown,
    CInputGroupText,
    CLabel,
    CSelect,
    CRow,
    CSwitch,
    CImg
  } from '@coreui/react'
  import { DocsLink } from 'src/reusable'
import './config.css'

const Config = () => {
  const [collapsed, setCollapsed] = React.useState(true)
  const [showElements, setShowElements] = React.useState(true)

  return (
      
<CRow>
<CCol xs="12" sm="6">
          <CCard>
            <CCardHeader>
             Configuration
             <DocsLink name="-Input"/>
            </CCardHeader>
            <CCardBody>        

<table>
  <tbody >
    <tr>
      <td width='70px'><input type="checkbox" style={{width:"18px", height:"18px"}}></input></td>
      <label style={{fontSize:"20px"}}>Factor confidence level</label>
   </tr>
    <tr>
        <td>
            <input type="range" min="1" max="120" id="myRange" onchange="updateTextInput(this.value);"></input>
            
            </td>
        <label style={{fontSize:"20px"}}>Frame Analysed Per Second</label>
            </tr>
    </tbody>
    </table>




            </CCardBody>
          </CCard>
        </CCol>
</CRow>
   )
  }
    
  
          
  export default Config

